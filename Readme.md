## Hello Skill with Kotlin

To create the skill `.zip` file can be deployed on AWS Lambda do the following:

```bash
./gradlew clean && ./gradlew build
```

For more information look at the `build.gradle` file.