package com.rahulrav

import com.amazon.speech.slu.Slot
import com.amazon.speech.speechlet.*
import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler
import com.amazon.speech.ui.PlainTextOutputSpeech
import com.amazon.speech.ui.Reprompt
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*

class HelloSpeechlet : Speechlet {

  companion object {
    val HELLO_INTENT = "HelloIntent"
    val SLOT_SUBJECT = "subject"

    val ALEXA_HELP = "AMAZON.HelpIntent"
    val ALEXA_STOP = "AMAZON.StopIntent"
    val ALEXA_CANCEL = "AMAZON.CancelIntent"

    // 15 s timeout
    val TIME_OUT = 15000L
  }

  lateinit var logger: Logger
  lateinit var client: BasicClient

  constructor() {
    logger = LoggerFactory.getLogger(HelloSpeechlet::class.java)
  }

  override fun onSessionEnded(request: SessionEndedRequest?, session: Session?) {
    logger.info("Session ended with Request id (${request?.requestId}) and Session id (${session?.sessionId})")
  }

  override fun onSessionStarted(request: SessionStartedRequest?, session: Session?) {
    logger.info("Session started with Request id (${request?.requestId}) and Session id (${session?.sessionId})")
  }

  override fun onIntent(request: IntentRequest?, session: Session?): SpeechletResponse? {
    val intent = request?.intent
    val name = intent?.name

    when (name) {
      ALEXA_HELP -> return buildMessage(
          "Welcome to Hello. Please say something to continue.",
          "Please say something to continue ?" /* reprompt */
      )
      HELLO_INTENT -> return handleHelloIntent(request!!)
      ALEXA_CANCEL, ALEXA_STOP -> return buildMessage("Goodbye.", shouldEndSession = true)
      else -> {
        logger.info("Ignoring intent with $name.")
        return null
      }
    }
  }

  private fun handleHelloIntent(request: IntentRequest): SpeechletResponse? {
    val query: Slot? = request.intent.slots[SLOT_SUBJECT]
    if (query != null && query.value != null) {
      val startTime = System.currentTimeMillis()
      logger.info("Handling query: ${query.value}")
      val result = client.handleRequest(query.value)
      val speechResponse = result.map { response ->
        val totalTime = System.currentTimeMillis() - startTime
        logger.info("Query response: $response (took $totalTime ms.)")
        when (response) {
          BasicClient.Result.Error -> buildMessage("I am sorry, but there was an error. Can you please try that again ?")
          is BasicClient.Result.Success -> {
            buildMessage(response.contents)
          }
        }
      }

      try {
        return speechResponse.await(TIME_OUT)
      } catch (exception: Exception) {
        logger.error("Error with speech response", exception)
        return buildMessage("Sorry. Something unexpected happened, please try again later")
      }
    } else {
      return buildMessage(
          "Hmm, I couldn't understand your request.",
          "Can you please try that again ?" /* reprompt */
      )
    }
  }

  override fun onLaunch(request: LaunchRequest?, session: Session?): SpeechletResponse? {
    val requestId = request?.requestId
    val sessionId = session?.sessionId
    logger.info("Launch request with Request id ($requestId) and Session id ($sessionId)")
    return buildMessage(
        "Welcome to Hello Intents. Please say something to continue.",
        "Please say something to continue ?" /* reprompt */
    )
  }

  private fun buildMessage(message: String, shouldEndSession: Boolean = true): SpeechletResponse {
    val speechText = PlainTextOutputSpeech()
    speechText.text = message
    val response = SpeechletResponse.newTellResponse(speechText)
    response.shouldEndSession = shouldEndSession
    return response
  }

  private fun buildMessage(message: String, repromptMessage: String): SpeechletResponse {
    val speechText = PlainTextOutputSpeech()
    speechText.text = message
    val repromptSpeech = PlainTextOutputSpeech()
    repromptSpeech.text = repromptMessage
    val reprompt = Reprompt()
    reprompt.outputSpeech = repromptSpeech
    val response = SpeechletResponse.newAskResponse(speechText, reprompt)
    return response
  }

}

/**
 * The main Speechlet request handler.
 * The name of the handler in the configuration should be <code>com.rahulrav.HelloSpeechletRequestHandler::handleRequest</code>.
 */
class HelloSpeechletRequestHandler : SpeechletRequestStreamHandler(HelloSpeechlet(), HelloSpeechletRequestHandler.SUPPORTED_IDS) {
  companion object {
    val SUPPORTED_IDS = HashSet<String>()
    val APPLICATION_ID = "<<your_application_id>>"

    init {
      SUPPORTED_IDS.add(APPLICATION_ID)
    }
  }
}
